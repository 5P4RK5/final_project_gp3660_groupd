package Game;

import java.util.ArrayList;
import java.util.Random;

import GameObjects.GameObject;
import GameObjects.Character;
import GameObjects.Background;
import GameObjects.Obstacles;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;


public class Controller {
	
	private float screenHeight,
				  screenWidth,
				  screenLimit;
	
	ArrayList<GameObject> drawableObjects;
	Background background;
	Character player;
	Obstacles[] hold;
	Obstacles currentObstacle;
			  //nextObstacle;
	
	private int numObstacles,
				points;
	
	private Sound jumpSound,
				  hurtSound;
	
	private Music backgroundMusic;
	
	private boolean paused,
					gameOver,
					gameStarted;
		
	public Controller(){

		screenHeight = Gdx.graphics.getHeight();
		screenWidth = Gdx.graphics.getWidth();
		screenLimit = screenWidth/3;;
		drawableObjects = new ArrayList<GameObject>(); 
		
		initSound();
		initBack();
		initChar();
		initObstacle();
		
		points = 0;
		
		paused = false;
		gameOver = false;
		gameStarted = false;
	}	

	private void initBack()
	{
		background = new Background(new Texture("mario_background.png"),0,0); //for now placing at 0 0
		background.addBackground(new Texture("mario_background2.png"));		
		drawableObjects.add(background);
	}	
	
	private void initObstacle()
	{
		numObstacles = 4;
		hold = new Obstacles[numObstacles];
		
		hold[0] = new Obstacles(new Texture("obstacle1.png"),Gdx.graphics.getWidth(),background.getFloor());
		
		hold[1] = new Obstacles(new Texture("obstacle2.png"),Gdx.graphics.getWidth(),(int)(background.getFloor()*2.5));
		hold[1].roll();
		
		hold[2] = new Obstacles(new Texture("obstacle3.png"),Gdx.graphics.getWidth(),(int)(background.getFloor()));
		hold[2].roll();
		
		hold[3] = new Obstacles(new Texture("obstacle4.png"),Gdx.graphics.getWidth(),(int)(background.getFloor()+50));
		hold[3].roll();
		
		currentObstacle = hold[new Random().nextInt(numObstacles)]; //uses anonymous Random object to get an int between 0 (inclusive ) and 3 (exclusive)
		drawableObjects.add(currentObstacle);
	}
	
	private void initChar()
	{
		player = new Character(new Texture("Standing1.png"), (int) screenWidth/10, background.getFloor());
		drawableObjects.add(player);
	}
	
	private void initSound()
	{
		jumpSound = Gdx.audio.newSound(Gdx.files.internal("Jumping.wav"));
		backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("backgroundMusic.ogg"));
		hurtSound = Gdx.audio.newSound(Gdx.files.internal("HitObject.wav"));
	}
	
	public void update()
	{
		processKeyboardInput();
		processMouseInput();

		if(gameStarted){
			for(int i=0; i<drawableObjects.size(); i++)
			{
				GameObject gObj = drawableObjects.get(i);
				
				if(!paused){	
					//moved player to the top; we want the player's inputs to register and update as soon as possible
					
					if(gObj instanceof Character) { ((Character) gObj).update(Gdx.graphics.getDeltaTime()); 
						if(player.sprite.getX() < 0-(player.sprite.getWidth()/1.2)) { player.setLives(0); gameOver = true; } //if player falls off back of screen
					}
					
					if(gObj instanceof Background) { ((Background) gObj).update(Gdx.graphics.getDeltaTime());
						if(player.isRunning() && background.getSpeed() < 200) { background.setSpeed(300); } //to make game start faster again after you stop running
						else if(player.isRunning() && player.sprite.getX() + player.sprite.getWidth() < (screenLimit)) { background.setSpeed(background.getSpeed()+3.5f); }
						else if(!player.isDoingSomething() && player.getSpeed() < background.getSpeed()) { background.setSpeed(background.getSpeed()-10); } //fix sliding problem
					}
					
					if(gObj instanceof Obstacles) { ((Obstacles) gObj).update(Gdx.graphics.getDeltaTime());
						//make object move a little faster than background if it is rotating
						if( ((Obstacles)gObj).isRolling() ) { currentObstacle.setSpeed(background.getSpeed()+100); }
						else { currentObstacle.setSpeed(background.getSpeed()); } //object is on the ground
						
						if( player.sprite.getBoundingRectangle().overlaps(gObj.sprite.getBoundingRectangle()) ) 
						{ 
							hurtSound.play(0.5f);
							
							player.setLives(player.getLives()-1);
							if(player.getLives() == 0) gameOver = true;
							
							//adjustment made after discussing with Shawn
							player.sprite.setX(player.sprite.getX()-50);
							gObj.sprite.setX(screenWidth);
							
							if(points >= 50) //prevent negatives
							 		points -= 50;

							background.setSpeed(background.getSpeed()*.6f);
						}
						else if(gObj.sprite.getX() < -gObj.sprite.getWidth()){ //obstacle falls off the screen, put next one at the end
							points += 100;
							drawableObjects.remove(i);//remove object from array
						
							currentObstacle = hold[new Random().nextInt(numObstacles)];
							drawableObjects.add(currentObstacle);
							
							gObj.sprite.setX( Gdx.graphics.getWidth() ); //set obstacle at end of screen
						}
					}//end of obstacle related checking	
				}//end of pause checking
			}//end of updating objects
		}//end of gameStarted condition
		
	}//end of update()
	
	private void processMouseInput(){}
	
	private void processKeyboardInput(){
		if (Gdx.app.getType() != ApplicationType.Desktop) return; // Just in case :)
		
		if(gameStarted){
			if(!paused){
				if(Gdx.input.isKeyPressed(Keys.RIGHT) && !player.isJumping()){ player.run(); }
				else 
					player.stopRunning();
			
				if(Gdx.input.isKeyJustPressed(Keys.UP) && !player.isJumping()) 
				{
					player.jump(screenHeight*.7f); 
					jumpSound.play(0.5f);
				}
				
				if(Gdx.input.isKeyPressed(Keys.DOWN) && !player.isJumping()) { player.dodge(); }
				
			}//end of !paused
			
			if(Gdx.input.isKeyJustPressed(Keys.P)) { 
				paused = !paused; 
				
				if(paused)
					backgroundMusic.pause();
				else
					backgroundMusic.play();
			}
			
			if(Gdx.input.isKeyJustPressed(Keys.M)) {
				if(backgroundMusic.isPlaying())
					backgroundMusic.pause();
				else
					backgroundMusic.play();
			}
			
			if(Gdx.input.isKeyJustPressed(Keys.R)){
				drawableObjects.clear(); //we need to clear because w/e objects we put in previous runs of the game remain
				backgroundMusic.stop();
								
				initBack();
				initObstacle();
				initChar();
				player.resetLives();
				
				points = 0;
				
				backgroundMusic.play();
				
				paused = false;
				gameOver = false;
			}
			
		}//end of GameStarted condition
				
		else /*if(!gameStarted) this will only ever be true once*/
			if(Gdx.input.isKeyPressed(Keys.SPACE)) 
			{ 
				backgroundMusic.setLooping(true);
				backgroundMusic.play();
				backgroundMusic.setVolume(0.5f);
				gameStarted = true; 
			}
		
	}//end of keyboard input checking
	
	public int getPoints(){ return points; }
	public void resetScore(){ points =0; }
	public boolean gameStarted(){ return gameStarted; }
	public boolean isGameOver(){ return gameOver; }
	public boolean isGamePaused(){ return paused; }
	public void pauseGame(boolean c) { paused = c; }
		
	public ArrayList<GameObject> getDrawableObjects(){ 	return drawableObjects; }
	
}//end of class
