package Game;


import GameObjects.GameObject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import com.badlogic.gdx.graphics.Texture;

public class Renderer {

	private SpriteBatch spriteBatch;
	private Controller control;
	BitmapFont font;//To display the score
	private String scoreName; //To store the scoreString
	
	Texture gameIntro,
			gameOver,
			pauseScreen,
			lives;
	
	Animation runAnim, jumpAnim;
	Texture runningSheet, jumpingSheet;
	TextureRegion[] runningFrames, jumpingFrames;
	TextureRegion currentRunFrame, currentJumpFrame;
	float runStateTime, jumpStateTime;
					
	public Renderer(Controller c){
		control = c;
		
		scoreName = "Score: " + c.getPoints();				
		font = new BitmapFont();
		font.setColor(1.0f, 1.0f, 1.0f, 1.0f);
			
		spriteBatch = new SpriteBatch(); 		
				
		gameIntro = new Texture(Gdx.files.internal("IntroScreen.png"));
		gameOver = new Texture(Gdx.files.internal("GameOver.png"));
		pauseScreen = new Texture(Gdx.files.internal("Pause.png"));
		lives = new Texture(Gdx.files.internal("Lives.png"));
		
		runningSheet = new Texture(Gdx.files.internal("Running.png"));		
		TextureRegion[][] temp = TextureRegion.split(runningSheet, runningSheet.getWidth()/4, runningSheet.getHeight()/2);		
		runningFrames = new TextureRegion[8];
				
		int index = 0;
		for(int rows = 0; rows < 2; rows++)
			for(int cols = 0; cols < 4; cols++)
				runningFrames[index++] = temp[rows][cols];
		
		runAnim = new Animation(c.player.getAnimSpeed(), runningFrames);
		runStateTime = 0f;
		
		index = 0;
		
		jumpingSheet = new Texture(Gdx.files.internal("Jumping.png"));
		TextureRegion[][] jtemp = TextureRegion.split(jumpingSheet,jumpingSheet.getWidth()/2,jumpingSheet.getHeight()/2);
		jumpingFrames = new TextureRegion[4];
				
		for(int rows = 0; rows<2; rows++)
			for(int cols =0; cols<2; cols++)
				jumpingFrames[index++] = jtemp[rows][cols];
		
		jumpAnim = new Animation(c.player.getAnimSpeed(),jumpingFrames);	
		jumpStateTime= 0f;
	}
	
	//use controller to take/store information from Character and Background to render here
	public void render()
	{
		spriteBatch.begin();	
		renderBackground();
			
		if(control.gameStarted())
		{			  
			for(GameObject gObj : control.getDrawableObjects()) { gObj.sprite.draw(spriteBatch); }
			renderLives();
			
			if(!control.isGameOver() && !control.isGamePaused()){		
				/*Increment score by 10 as long as game is running or as long as player is alive.
				 * Maybe later will implement a score reduction if player is hit with obstacle or dies losing lives.*/
				if(control.player.isDoingSomething()){
					scoreName = "Score: " + control.getPoints(); ; 
				} //additional condition: Only update score if he is doing something
				
				renderScore();
				
				//animate running; add later: if player's location is less than half screen, make him move fwd till he is half screen
				if( control.player.isRunning() && !runAnim.isAnimationFinished(runStateTime)){
					runStateTime += Gdx.graphics.getDeltaTime();
					currentRunFrame = runAnim.getKeyFrame(runStateTime, false);
									
					spriteBatch.draw( currentRunFrame, control.player.sprite.getX()-60/*to align animation w/hurtbox*/,control.player.sprite.getY() );
					
					//if character is running, but animation reached last frame
					if( runAnim.isAnimationFinished(runStateTime) )
						runStateTime = 0f;
				}//end of run animation
				
				else if( control.player.isJumping() && !jumpAnim.isAnimationFinished(jumpStateTime)){								
					jumpStateTime += Gdx.graphics.getDeltaTime();
					currentJumpFrame = jumpAnim.getKeyFrame(jumpStateTime,false);
					spriteBatch.draw(currentJumpFrame, control.player.sprite.getX()-100/*to align hurtbox animation w/hurtbox*/, control.player.sprite.getY()-25);
					
					if( jumpAnim.isAnimationFinished(jumpStateTime) )
						jumpStateTime = 0f;					
				}//end of jump animation
				
				else if( control.player.isDodging() )
					spriteBatch.draw(control.player.getDodgeFrame(), control.player.sprite.getX()-30, control.player.sprite.getY());				
			}//end of gameRunning and not paused or gameOver condition
						
			else if(control.isGamePaused() || control.isGameOver()){
				
				if(control.player.isRunning() && !control.player.isJumping()) 
					spriteBatch.draw( currentRunFrame, control.player.sprite.getX()-50,control.player.sprite.getY() );
			
				else if(control.player.isJumping() && !control.player.isRunning())
				{
					spriteBatch.draw(currentJumpFrame, control.player.sprite.getX()-100, control.player.sprite.getY()-25);
				}
				
				else if( control.player.isDodging() )
					spriteBatch.draw(control.player.getDodgeFrame(), control.player.sprite.getX(), control.player.sprite.getY());
				
				else
					spriteBatch.draw( control.player.getFrame(), control.player.sprite.getX(), control.player.sprite.getY() );
								
				if(control.isGamePaused() && !control.isGameOver()) 
				{ 
					renderScore(); //Want to still display score when game is paused
					spriteBatch.draw(pauseScreen, Gdx.graphics.getWidth()/2 - pauseScreen.getWidth()/2, Gdx.graphics.getHeight()/2 - pauseScreen.getHeight()/2); 
				}
				
				else if(control.isGameOver()) 
				{ 
					control.resetScore();
					renderScore();
					spriteBatch.draw(gameOver, Gdx.graphics.getWidth()/2 - gameOver.getWidth()/2, Gdx.graphics.getHeight()/2 - gameOver.getHeight()/2); 
					control.pauseGame(true); 
				}			
				
			}//end of game over or game paused
						
		}//end of GameStarted condition
		
		else //if game has not started; This will only ever be false once like gameStarted in controller
			spriteBatch.draw(gameIntro, Gdx.graphics.getWidth()/2 - gameIntro.getWidth()/2, Gdx.graphics.getHeight()/2 - gameIntro.getHeight()/2);
					
		spriteBatch.end();
	}//end of render	
	
	public void renderBackground() { spriteBatch.draw(control.background.getNextBackground(),control.background.getNextBgXPos(), 0); }
	public void renderScore() { font.draw(spriteBatch, scoreName, Gdx.graphics.getWidth()-100, Gdx.graphics.getHeight()-18); }
	
	public void renderLives(){
		for(int i = 0; i < control.player.getLives(); i++)
			spriteBatch.draw(lives,lives.getWidth()*i, Gdx.graphics.getHeight()- lives.getHeight());
	}
		
}//end of class
