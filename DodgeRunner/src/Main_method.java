import Game.TheGame;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;


//so the main calls game. 

public class Main_method {

	public static void main (String[] arg)
	{
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "DodgeRunner";
		//resized to the common mobile device ratio of 16:9
		config.width = 1200;
		config.height = 675;
		config.resizable = false;
		new LwjglApplication(new TheGame(), config);
	}
	
}
