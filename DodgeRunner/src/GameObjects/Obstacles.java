package GameObjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Obstacles extends GameObject implements Updatable {

	//private Texture texture;
	
	private float rotDeg;
	
	public Obstacles(Texture texture, int x, int y)
	{
		sprite = new Sprite(texture);
		sprite.setPosition(x, y);
		
		rotDeg = 0;

		setSpeed(100);
		setIsDrawable(true);
	}
	
	public void roll() { rotDeg = 1080; }
	public boolean isRolling() {
		if(rotDeg > 0) return true;
		else return false;
	}
			
	@Override
	public void update(float deltaTime) {
		sprite.rotate(rotDeg*deltaTime);
		sprite.setX(sprite.getX() - getSpeed()*deltaTime);
	}

}
