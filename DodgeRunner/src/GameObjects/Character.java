package GameObjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Character extends GameObject implements Updatable {

	boolean running, //temp; added to test running using Animation class
			dodging,
			jumping,
			landing;
		
	private float holdX, 
				  holdY,
				  maxJumpHeight,
				  screenLimit,
				  animSpeed; //this is a value is how fast the character will be animated in Renderer
	
	private int lives;
		
	private Texture currentFrame,
					standingFrame,
					dodgeFrame,
					dodgeHurtbox,
					jumpHurtbox,
					hurtbox; //see change log for explanation

	public Character(Texture texture, int x, int y)
	{
		standingFrame = texture;		
		sprite = new Sprite(standingFrame);
		currentFrame = standingFrame;		
		hurtbox = new Texture(Gdx.files.internal("HurtBox.png"));
		
		dodgeFrame =  new Texture(Gdx.files.internal("Dodge.png"));		
		dodgeHurtbox = new Texture(Gdx.files.internal("DodgeHurtBox.png"));
		
		jumpHurtbox = new Texture(Gdx.files.internal("JumpingHurtBox.png"));
			
		//hold values are to store the character's X and Y when switching sprites
		holdX = x;
		holdY = y;
		screenLimit = Gdx.graphics.getWidth()/3;
		
		lives = 5;
		
		sprite.setOrigin(texture.getWidth()/2, texture.getHeight()/2);
		sprite.setPosition(x, y);
		
		running = false;
		jumping = false;
		landing = false;
		
		animSpeed = 0.04f; //animation speed		
		setSpeed(100); //movement speed
		
		setIsDrawable(true);
	}

	//public void setFrame(Texture frame) { currentFrame = frame; }
	public Texture getFrame() { return currentFrame; }
	public Texture getDodgeFrame() { return dodgeFrame; }
	
	public void resetLives() { lives = 5; }
	public void setLives(int val) { lives = val; }
	public int getLives() { return lives; }
	
	public void setAnimSpeed(float val) { animSpeed = val; }
	public float getAnimSpeed() { return animSpeed; }
	
	public boolean isDoingSomething() {
		if(running || jumping || landing|| dodging)
			return true;
		else 
			return false;
	}
	
	public boolean isRunning() { return running; }		
	public void run() {
		if(!running){ //added for computational efficiency; holding " -> " causes all of this to constantly happen
			setSpeed(100);
			running = true;
			dodging = false;
			
			holdX = sprite.getX();
			holdY = sprite.getY();
			
			sprite = new Sprite(hurtbox);
			sprite.setAlpha(0);
			sprite.setPosition(holdX,holdY);
			currentFrame = hurtbox;
		}
	}
	public void stopRunning() { 
		if(running){
			running = false;
			
			holdX = sprite.getX();
			holdY = sprite.getY();
			
			sprite = new Sprite(standingFrame);
			sprite.setPosition(holdX,holdY);
			currentFrame = standingFrame;
		}	
	}	
	
	public float getJumpHeight() { return maxJumpHeight; }
	public boolean isJumping() { return jumping; }
	public void jump(float height) {
		if(!jumping){//if not already jumping
			setSpeed(4000); //made jump faster to compensate for human reaction time
			jumping = true;
			running = false; //don't let the animation for running go while jumping
			dodging = false;
			
			maxJumpHeight = height;
			holdX = sprite.getX();
			holdY = sprite.getY(); //holdY will be used later to determine when he comes back to where he was before jumping
			
			sprite = new Sprite(jumpHurtbox);
			sprite.setAlpha(0);
			sprite.setPosition(holdX, holdY);
			currentFrame = jumpHurtbox;
		}
	}
	
	public boolean isDodging() { return dodging; }
	public void dodge() {
		if(!dodging && !jumping){
			setSpeed(75);
			dodging = true;
			running = false;
			jumping = false;
			
			holdX = sprite.getX();
			holdY = sprite.getY();
			
			sprite = new Sprite(dodgeHurtbox);
			sprite.setAlpha(0);
			sprite.setPosition(holdX, holdY);
			currentFrame = dodgeHurtbox;
		}
	}
	
	@Override
	public void update(float deltaTime) {
		
		if(running && !jumping && sprite.getX()+sprite.getWidth() < screenLimit)
			sprite.translate(getSpeed()*deltaTime, 0);
												
		else if(jumping && sprite.getY() < maxJumpHeight && !landing){
			if(sprite.getX() + sprite.getWidth() + 250*deltaTime < screenLimit)
				sprite.translate(300*deltaTime, getSpeed()*deltaTime); //only translate if it won't push him past half screen
			else
				sprite.translate(0, getSpeed()*deltaTime);
			
			//if the next rep of update would make the character translate past the max height of his jump, start landing
			if(sprite.getY() + getSpeed()*deltaTime > maxJumpHeight)
				landing = true;
		}
		
		else if(landing){
			setSpeed(800);
			
			if(sprite.getX() + sprite.getWidth() + 250*deltaTime < screenLimit)
				sprite.translate(200*deltaTime, -getSpeed()*deltaTime); 
			else
				sprite.translate(0, -getSpeed()*deltaTime);		
			
			//if the next rep of update would make character translate below where he was when he jumped
			if(sprite.getY() - getSpeed()*deltaTime < holdY){
				setSpeed(50);
				
				jumping = false;
				landing = false;
						
				holdX = sprite.getX();
				
				sprite = new Sprite(standingFrame);
				sprite.setPosition(holdX, holdY);
				currentFrame = standingFrame;
				
			}
		}			
		
		if(!running && !jumping) //if he isn't running, let him slide with the background...
			//sprite.translate(-3*getSpeed()*deltaTime, 0);
			sprite.translate(-1*getSpeed()*deltaTime, 0);
			
	}
}//end of class

