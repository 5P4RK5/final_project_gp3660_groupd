package GameObjects;
import com.badlogic.gdx.graphics.g2d.Sprite;

public abstract class GameObject {	
	public Sprite sprite;
	
	private boolean drawable;
	private boolean updatable;
		
	private float speed; //added by Shadiq. This might prove to be really helpful in the future
		
	public GameObject(){}
	
	public boolean isDrawable(){ return drawable; }
	public boolean isUpdatable(){ return updatable; }
	
	public void setIsDrawable(boolean val){ drawable = val; }
	public void setIsUpdatable(boolean val){ updatable = val; }
	
	//added by Shadiq
	public void setSpeed(float newSpeed) { speed = newSpeed; }
	public float getSpeed(){ return speed;}
}
