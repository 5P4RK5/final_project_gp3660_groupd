package GameObjects;

import java.util.ArrayList;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Background extends GameObject implements Updatable{
	
	private int floor; //# of pixels from the bottom of the image where we want the character to walk on
	private float  nextBgXPos;
	private int scrollComplete; //used for increasing the speed
	
	ArrayList<Texture> bgTextures; //for future use in case we get more backgrounds
			
	public Background(Texture texture, int x, int y)
	{
		bgTextures = new ArrayList<Texture>();
		sprite = new Sprite(texture);
		scrollComplete = 0;
		floor = (int)sprite.getHeight()/15;
		
		nextBgXPos = sprite.getWidth();
		setSpeed(300);//"speed" = how many pixels x changes by
		
		setIsDrawable(true);
	}
	
	public void setFloor(int floorVal) { floor = floorVal; }
	public int getFloor() { return floor; }
	
	//for Controller and Renderer
	public float getXPos() { return sprite.getX(); }
	public float getNextBgXPos() { return nextBgXPos; }
	
	/*Will use this so I can make the different backgrounds appear*/
	public void addBackground(Texture texture)
	{
		bgTextures.add(texture);
	}
	public Texture getNextBackground() { return bgTextures.get(bgTextures.size()-1); }
			
	@Override
	public void update(float deltaTime) {
		
		//if background has not completely fallen off, both backgrounds x -= speed*deltaTime
		if( sprite.getX() < nextBgXPos && sprite.getX() > -sprite.getWidth() ){
			sprite.translate(-1*getSpeed()*deltaTime,0); //sprite.setX(getXPos() - getSpeed()*deltaTime);					
			nextBgXPos = sprite.getX() + sprite.getWidth();
		}
		//if first background has fallen off, swap the sprite texture with the following background texture and reset the xPos so previous if statement is true again
		if( sprite.getX() < -sprite.getWidth() ){
			Texture hold = sprite.getTexture();
			float holdX = sprite.getX();
			
			sprite = new Sprite( getNextBackground() );
			sprite.setPosition(holdX+sprite.getWidth(), 0f);
			
			bgTextures.remove(0);
			bgTextures.add(hold);
			nextBgXPos = sprite.getWidth();
			
			scrollComplete++;
			if(scrollComplete == 3){ setSpeed(getSpeed()+10); } //after four scroll passes increase speed
	
		}
			
	}

}//end of class