package GameObjects;

public interface Updatable {

	void update(float deltaTime);
	
}
